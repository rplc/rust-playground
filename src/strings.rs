/**
 * primitive string: immutable, fixed-length, memory
 * string: growable, heap-allocated
 */

pub fn run() {
    let foo = "Foo"; //primitive string
    let mut hello = String::from("Helllo ");

    println!("{}, {}", foo, hello);

    // length
    println!("length: {}", foo.len());

    // appending
    hello.push('W'); // push char
    hello.push_str("0oOo0oO"); // push string
    println!("{}", hello);

    // capacity in bytes
    println!("Cap: {}", hello.capacity());

    // empty
    println!("empty: {}", hello.is_empty());

    // contains
    println!("contains: {}", hello.contains("elllo"));

    // loop
    for word in hello.split_whitespace() {
        println!("{}", word);
    }

    // asserting; throw error if not passed
    // assert_eq!(2, 3);
}