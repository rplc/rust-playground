pub fn run() {
    let age = 21;
    let check_id = false;

    // if/else
    if age >= 21 && check_id {
        println!("Old and id");
    } else {
        println!("Nope");
    }

    // shorthand if
    let of_age = if age >= 21 { true } else { false };
    println!("of age: {}", of_age);
}