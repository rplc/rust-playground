pub fn run() {
    greeting("Hello", "Foo");

    let sum = add(5, 5);
    println!("{}", sum);

    // Closure functions => can use block scoped variables, see n3
    let n3 = 10;
    let add_nums = |n1: i32, n2: i32| n1 + n2 + n3; 
    println!("Closure sum: {}", add_nums(3, 3))
}

fn greeting(greet: &str, name: &str) {
    println!("{} {}", greet, name);
}

fn add(n1: i32, n2: i32) -> i32 { // -> i32 = return type
    n1 + n2 // no semicolon = return value
}