// vectors = resizable arrays

use std::mem;

pub fn run() {
    let mut numbers: Vec<i32> = vec![1, 2, 3, 4, 5];
    // let numbers: [i32; 5] = [1, 2, 3, 4]; // <- fails!

    println!("{:?}, second entry: {}, length: {}", numbers, numbers[1], numbers.len());

    // reassign
    numbers[1] = 20; // potentially unsecure; thread will panic if out of bounce; better use getter/setter
    println!("{:?}", numbers);

    // stack allocated
    // println!("Array occupies {} bytes", std::mem::size_of_val(&numbers)); // funktionier ohne use Z. 3
    println!("Array occupies {} bytes", mem::size_of_val(&numbers)); // functioniert nur mit use Z. 3

    // Slice
    let slice: &[i32] = &numbers[1..4];
    println!("slice: {:?}", slice);

    // additional to array

    // push
    numbers.push(6);
    println!("{:?}", numbers);

    numbers.pop();
    println!("{:?}", numbers);

    // Loop
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    // Loop & Mutate
    for x in numbers.iter_mut() {
        // x is a mutable reference; for accessing and modifying it we need to add an * infront of it (therefore *x)
        // further explanation see: https://doc.rust-lang.org/1.8.0/book/references-and-borrowing.html#mut-references
        *x *= 2; // or *x = *x * 2;
    }
    println!("{:?}", numbers);

}