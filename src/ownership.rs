// https://youtu.be/8M0QfLUDaaA

pub fn run() {
    dropping();
    borrowing();
}

/**
 * variables/arguments that the current scope owns will be destroyed/dropped when the scope ends; here:
 * string x is passed in drop_string method (so the drop_string scope owns the variable), drop_string methode
 * scope ends (and the string is not returned -which would change the ownership back to the dropping method-)
 * therefore the string x gets destroyed and is no longer accessible in the dropping scope
 */
fn dropping() {
    let x = String::from("Hello World!");

    println!("{}", x);

    drop_string(x);

    // println!("{}", x); // fails because string x was destroyed in drop_string
}

fn drop_string(s: String) {}
/* might be fixed via (and let y = drop_string(x))
fn drop_string(s: String) -> String {
    s
}
*/

fn borrowing() {
    // borrow a READ ONLY version of x; as long as z or y exist all THREE are IMmutable!!! -> Solution limit existance of z and y
    let mut x = String::from("Hello");
    //let z = &x;
    //let y = &x;

    { // creates custom scope
        let z = &x;
        // x.push('x'); // not allowed, is not mutable because it was borrowed
        let y = &x;
    } // borrow of x ends -> x can be mutated
    x.push('x'); // allowed, because all references to x were cleaned up (got out of scope)

    let mut a = String::from("World");
    {
        // borrows a mutable version of a
        let b = &mut a;
        // the ONLY way to access the data is through b; even a is not accessible!
    }
}

/**
 * example function takes the ownership of a; b is borrowed read only; c is borrowed mutable
 */
fn example(a: String, b: &str, c: &mut str) {}