// Rust is block scoped
// Variables are immutable by default

pub fn run() {
    let name = "Brad";
    let mut age = 10;

    println!("Name: {}, Age: {}", name, age);

    age = 11;

    println!("Name: {}, Age: {}", name, age);

    // Define constants, need to explizitly define the type of the constant
    const ID: i32 = 1;
    println!("ID: {}", ID);

    // assigning multiple vars
    let (name_a, age_b) = ("Brad", 12);
    println!("{} is {}", name_a, age_b);
}