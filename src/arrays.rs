// array = fixed length

use std::mem;

pub fn run() {
    let mut numbers: [i32; 5] = [1, 2, 3, 4, 5];
    // let numbers: [i32; 5] = [1, 2, 3, 4]; // <- fails!

    println!("{:?}, second entry: {}, length: {}", numbers, numbers[1], numbers.len());

    // reassign
    numbers[1] = 20;
    println!("{:?}", numbers);

    // stack allocated
    // println!("Array occupies {} bytes", std::mem::size_of_val(&numbers)); // funktionier ohne use Z. 3
    println!("Array occupies {} bytes", mem::size_of_val(&numbers)); // functioniert nur mit use Z. 3

    // Slice
    let slice: &[i32] = &numbers[1..4];
    println!("slice: {:?}", slice);

    for n in numbers {
        println!("Number: {}", n);
    }
    // simplifiziert:
    numbers.iter().for_each(|n| println!("Number: {}", n));
}