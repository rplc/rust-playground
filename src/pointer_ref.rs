pub fn run() {
    // primitives array -> duplication
    let mut arr1 = [1, 2, 3];
    let arr2 = arr1;
    arr1[1] = 20;

    println!("{:?}", (arr1, arr2));

    // non primitives
    let vec1 = vec![1, 2, 3];
    //let vec2 = vec1; // moves value of vec1 to vec2 -> next usage of vec1 is invalid
    let vec2 = &vec1; // moves value of vec1 to vec2

    println!("{:?}", (&vec1, vec2)); // need & before vec1 because its borrowed (see vec2 = &vec1)
}