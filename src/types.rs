/**
 * Integers: i8, i16, i32, i64, i128 (auch als u8/16/... -unsinged- verfügbar; number = number of bits)
 * Floats: f32, f64
 * Boolean
 * Characters
 * Tuples (mixed array/list)
 * Arrays (fixed length)
 */

pub fn run() {
    // default is i32
    let x = 1;

    // default is f64
    let y = 2.5;

    // explizit type
    let y: i64 = 1123123123123123123; // note: declaring y a second time is not a problem

    // max value
    println!("max i32: {}", std::i32::MAX);
    println!("max i64: {}", std::i64::MAX);

    // bool
    let a: bool = false; // type optional
    let b = 10 > 5;

    println!("{}, {}", a, b);

    // chars
    let c = '\u{1F600}';
    let d = 'a';
    println!("{}, {}", c, d);

}