// from https://youtu.be/vqavdUGKeb4

// Null Pointer
fn f(pointer: Option<&Foo> /* Pointer-Reference (&Foo) might never be null, if null pointer is wished, add Option around it*/) {
    match pointer {
        Some(ptr) => ptr.g(),
        None = {}
    }
}

// Match
// enums might have values assigned to them
// Option ist auch nur ein Enum
enum Foo { A(i32), B, C }

fn do_match(ptr: &Foo) {
    let foo = match ptr {
        Foo::A(n) => n,
        _ => 0
    };

    // ...
}

// result
fn h1() {
    let input = 10;

    match h(input) {
        Ok(result) => println!("Result: {}", result),
        Err(e) => println!("Error: {}", e)
    }

    // alternativ
    let i = match h(3) {
        Ok(i) => i,
        // returns the error, return type of function should/must be result; says something like, I do not handle the error but my caller should
        err => return err
    }
    //shorthand (i will be bound, if error will be thrown)
    let i = h(3)?;
}

fn h(i: i32) -> Result<i32, String> {
    match i {
        i if i >= 0 => Ok(i + 10),
        _ => Err(format!("Input to h less than 0, found: {}", i))
    }
    // TODO better use an enum as error type, see below
}

// helper functions
fn he(y: Option<i32>) -> Option<i32> {
    match y {
        Some(yy) => Some(add_four(yy)), // just some method call
        None => None
    }
    // equivalent to:
    y.map(add_four)
    // inline closure function
    y.map(|x| x + 4)
    // .and, .and_then, .or, .or_else

    // Option.ok_or to convert from option to result
}

// and_then example
fn check_positive(input: Option<i32>) -> Option<i32> {
    input.and_then(|i| {
        if i < 0 {
            None
        } else {
            Some(i)
        }
    })

    // alternative:
    input.filter(|i| i >= 0)
}

// iterator
fn vec_it() {
    let vec = vec![0,1,2,3];

    for (i, v) in vec.iter() // get iterator of vec
                     .chain(Some(42).iter()) // append another iterator to the previous one (in this case: [0, 1, 2, 3, 42])
                     .enumerate() { // return not an iterator but a tuple of (index, value)
        println!("{}: {}", i, v);
    }

    // collect creates vector/array/what ever out of an iterator; collect is very smart and adapts to the type of the variable.
    // so make sure to specify an EXPLICIT TYPE!!!
    let vec_2: Vec<_> = vec.iter().map(|x| x * 2).collect();
    let map: HashMap<_, _> = vec.iter().map(|x| x * 2).enummerate().collect();
}

// using enums as error type
// you can give all the information you want to the enum
// look into the failure libary, evtl schon in der std libary drinnen
pub enum MyError {
    Server(u8),
    User(String),
    Connection
}