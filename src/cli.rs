use std::env;

pub fn run() {
    // call with `cargo run hello`
    let args: Vec<String> = env::args().collect();

    println!("Args: {:?}", args); // first arg is always the executable

    let command = if args.len() > 1 { args[1].clone() } else { String::from("") };
    println!("Command: {}", command);
}