pub fn run() {
    println!("Hello from print.rs");

    // Basic Formatting
    let x = 10;
    println!("This is my {} number and my second {} arg.", x, "Fooo-BAAAR");

    // Positional Arguments
    println!("{0} bla {1} for {0}", 1, 2);

    // Named Arguments
    println!("{name} likes to {activity}", activity = "test", name = "Rolfo");

    // Placeholder for debug trait
    println!("{:?}", (12, true, "hello"));
}