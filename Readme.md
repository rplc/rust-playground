# Rust setup
- `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
- VS Code Extensions: "CodeLLDB" (Linux) bzw "C/C++" (Windows), "Rust (rls)"
- root folder: `cargo init`
- copy launch.json
- VS Code Settings: enable "Allow Breakpoints Everywhere"
- See also [link](https://www.forrestthewoods.com/blog/how-to-debug-rust-with-visual-studio-code/) and [link](https://github.com/editor-rs/vscode-rust/blob/master/doc/main.md)
- Build `cargo build`, F5 to run
- [Rust reading](https://doc.rust-lang.org/book/)

# Rust Know how
- rustup update (updates dependencies)
- rustc file (compiler)
- cargo (package manager und mehr)
  - cargo build -> baut nur
  - cargo run -> baut und lässt laufen
  - cargo build --release (prod version ~> ./target/release)#
- edition Keyword in cargo.toml -> different code-level versions (neue keywords à la async zb nur in edition 2021)
  - edition switch tool rustfix um sachen automatisiert zu portieren/zu fixen

- Sinnvoll weil
  - WebAssembly
  - Große Companies (Firefox, Amazon, Google, Facebook, Microsoft)

# Further Reading
- https://doc.rust-lang.org/rustdoc/what-is-rustdoc.html
  - vor allem auch Documentation tests = tests in the documentation
- https://github.com/rust-lang/rust-clippy
  - mehr warnungen/fehlermeldungen
